package com.work.present.jpcsousa.presentdemo.Fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.work.present.jpcsousa.presentdemo.MainActivity;
import com.work.present.jpcsousa.presentdemo.Manager.MeteoManager;
import com.work.present.jpcsousa.presentdemo.Object.Meteo;
import com.work.present.jpcsousa.presentdemo.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SearchFragment extends Fragment {

    private LinearLayoutManager mLayoutManager;
    private RecyclerView mList;
    private RecyclerViewSearchAdapter mAdapter;
    private EditText mSearchInputText;
    private Button mSearchButton;

    public static Fragment newInstance() {
        SearchFragment fragment = new SearchFragment();
        return fragment;
    }

    public SearchFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);

        mList = (RecyclerView) view.findViewById(R.id.searchList);
        mList.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.scrollToPositionWithOffset(2, 20);
        mList.setLayoutManager(mLayoutManager);
        mList.setAdapter(mAdapter = new RecyclerViewSearchAdapter());

        mSearchInputText = (EditText) view.findViewById(R.id.searchEntry);
        mSearchButton = (Button) view.findViewById(R.id.searchButton);

        ArrayList<Meteo> meteoArrayList = MeteoManager.getInstance().getSearchList();

        for (Meteo meteo : meteoArrayList)
            mAdapter.add(meteo);

        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String textSearch = mSearchInputText.getText().toString();
                if (!textSearch.equals("")) {
                    if (!isAllowed(textSearch)) {
                        new AlertDialog.Builder(getContext())
                                .setTitle("Alert")
                                .setMessage("Special characters are not allowed")
                                .setPositiveButton(R.string.understand, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (getView() != null) {
                                            mSearchInputText.setText("");
                                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                                        }
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    } else {
                        ArrayList<Meteo> meteoList = MeteoManager.getInstance().getSearchList();

                        if (getView() != null) {
                            mSearchInputText.setText("");
                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                        }

                        try {
                            ((MainActivity) MeteoManager.getInstance().getContext()).getURLContent(textSearch);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        return view;
    }


    @Override
    public void onPause(){
        if (getView()!=null) {
            mSearchInputText.setText("");
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        }
        super.onPause();
    }


    @Override
    public void onResume(){
        super.onResume();
        if (getView()!=null) {
            mSearchInputText.setText("");
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        }
    }

    private boolean isAllowed(String textSearch) {
        textSearch = textSearch.toLowerCase();
        Pattern p = Pattern.compile("[a-z]*");
        Matcher m = p.matcher(textSearch);

        if (m.find()){
            if (m.group().equals(textSearch))
                return true;
        }
        return false;
    }

    public void removeItemSearch() {
        mAdapter.remove();
    }

    public void updateAdapterList(Meteo meteo) {
        mAdapter.add(meteo);
        mAdapter.removeItemsAtView();
    }


    public void updateAdapterListComplete() {
        ArrayList<Meteo> meteoArrayList = MeteoManager.getInstance().getSearchList();
        for (int i =meteoArrayList.size()-1;i==0;i--){
            mAdapter.add(meteoArrayList.get(i));
        }
        mAdapter.removeItemsAtView();
    }


    public static class RecyclerViewSearchAdapter extends RecyclerView.Adapter<RecyclerViewSearchAdapter.ViewHolder> {
        public static List<Meteo> items = new ArrayList<Meteo>();
        Meteo notifClicked;

        public List<Meteo> getItems() {
            return items;
        }

        public Meteo getItemTouched(){
            return notifClicked;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.search_fragment_item, viewGroup, false);
            return new ViewHolder(view);
        }


        @Override
        public void onBindViewHolder(final ViewHolder viewHolder, int i) {
            final Meteo meteo = items.get(i);
            viewHolder.text.setText(meteo.getCity().getName());

            viewHolder.text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MeteoManager.getInstance().setActualMeteo(meteo);
                    ((MainActivity) MeteoManager.getInstance().getContext()).updateUIMeteoFragment();
                    Toast.makeText(MeteoManager.getInstance().getContext(),"City changed to "+
                            MeteoManager.getInstance().getActualMeteo().getCity().getName()
                            ,Toast.LENGTH_LONG).show();

                }
            });
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        public void add(Meteo meteo) {
            if (validInsertion(meteo)){
                items.add(0, meteo);
                notifyItemInserted(0);
            }
        }

        private boolean validInsertion(Meteo meteo) {
            for (Meteo meteoListItem  :items){
                if (meteo.getCity().getName().equals(meteoListItem.getCity().getName()))
                    return false;
            }

            return true;
        }

        public void removeAll(){
            try {
                items.removeAll(items);
            }catch (IndexOutOfBoundsException e){

            }
        }
        public void remove() {
            if (items.isEmpty())
                return;
            items.remove(0);
            notifyItemRemoved(0);
        }

        public void removeItemsAtView() {
            if (items.size() > 10) {
                for (int i = 10; i < items.size(); i++) {
                    items.remove(i);
                    notifyItemRemoved(i);
                }
            }
        }


        static class ViewHolder extends RecyclerView.ViewHolder {
            TextView text;

            public ViewHolder(final View itemView) {
                super(itemView);
                text = (TextView) itemView.findViewById(R.id.cityName);
                text.setTextColor(Color.WHITE);
                text.setTextSize(28);
            }
        }
    }
}
