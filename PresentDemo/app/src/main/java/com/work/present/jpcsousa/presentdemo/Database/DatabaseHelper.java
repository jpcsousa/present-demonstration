package com.work.present.jpcsousa.presentdemo.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;


import com.work.present.jpcsousa.presentdemo.Object.City;
import com.work.present.jpcsousa.presentdemo.Object.Meteo;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by JoaoSousa on 07/09/2015.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String LOG = "DatabaseHelper";

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "meteo_present";


    private static final String TABLE_METEO ="meteo_table";

    // COLUMN USER
    private static final String KEY_ID_METEO ="id_meteo";
    private static final String KEY_NAME ="name";
    private static final String KEY_TEMP ="temperature";
    private static final String KEY_IMAGE_SRC ="imagesrc";
    private static final String KEY_DESCRIPTION ="description";


    private static final String CREATE_TABLE_METEO = "CREATE TABLE " +TABLE_METEO
            +"("
            + KEY_ID_METEO + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KEY_NAME +" TEXT,"
            + KEY_TEMP+" TEXT,"
            + KEY_IMAGE_SRC+" TEXT,"
            + KEY_DESCRIPTION+" TEXT"
            +")";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // creating required tables
        db.execSQL(CREATE_TABLE_METEO);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_METEO);
        onCreate(db);
    }

    //CREATE CONTACTS
    public long createMeteoItem(Meteo meteo) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_NAME, meteo.getCity().getName());
        values.put(KEY_TEMP, meteo.getTemp());
        values.put(KEY_IMAGE_SRC, meteo.getImageSrc());
        values.put(KEY_DESCRIPTION, meteo.getDescription());

        long tableID = db.insert(TABLE_METEO, null, values);
        return tableID;
    }

    public ArrayList<Meteo> getAllMeteo(){
        ArrayList<Meteo> meteoList = new ArrayList<Meteo>();
        String selectQuery = "SELECT  * FROM " + TABLE_METEO;


        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                String name = c.getString(c.getColumnIndex(KEY_NAME));
                String temp = c.getString(c.getColumnIndex(KEY_TEMP));
                String img = c.getString(c.getColumnIndex(KEY_IMAGE_SRC));
                String desc = c.getString(c.getColumnIndex(KEY_DESCRIPTION));


                Meteo meteo= new Meteo(new City(0,name),temp,img,desc);
                meteoList.add(meteo);
            } while (c.moveToNext());
        }
        c.close();
        return meteoList;

    }

}
