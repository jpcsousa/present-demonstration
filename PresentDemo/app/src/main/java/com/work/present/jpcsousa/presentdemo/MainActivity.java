package com.work.present.jpcsousa.presentdemo;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.work.present.jpcsousa.presentdemo.Database.DatabaseHelper;
import com.work.present.jpcsousa.presentdemo.Fragments.MeteoFragment;
import com.work.present.jpcsousa.presentdemo.Fragments.SearchFragment;
import com.work.present.jpcsousa.presentdemo.GPS.GPSTracker;
import com.work.present.jpcsousa.presentdemo.Manager.MeteoManager;
import com.work.present.jpcsousa.presentdemo.Object.City;
import com.work.present.jpcsousa.presentdemo.Object.CurrentLocation;
import com.work.present.jpcsousa.presentdemo.Object.Meteo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private AdapterViewPager mAdapterViewPager;
    private ViewPager mViewPager;
    private Context mContext;
    private DatabaseHelper dbHelper;

    private boolean initiated = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComponents();
        initUIComponents();

        if (savedInstanceState!=null){
            initiated = savedInstanceState.getBoolean("initiated");
        }

        if (!initiated){
            try {
                setMeteoInformation();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            ArrayList<Meteo> lastSearch =  MeteoManager.getInstance().getSearchList();
            if (!lastSearch.isEmpty()){
                try {
                    getURLContent(lastSearch.get(lastSearch.size()-1).getCity().getName());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        MeteoManager.getInstance().setContext(this);
        Log.e("onr,", "");
    }

    @Override
    protected void onPause() {
        MeteoManager.getInstance().setContext(null);
        super.onPause();

    }

    @Override
    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putBoolean("initiated", initiated);
    }

    private void setMeteoInformation() throws IOException {

        if (MeteoManager.getInstance().getActualMeteo()==null){
            String city = getCurrentLocation();
            MeteoManager.getInstance().setCity(city);
        }

        getURLContent(MeteoManager.getInstance().getCity());
    }

    private void setMeteoInformationMenu() throws IOException {

        String city = getCurrentLocation();
        MeteoManager.getInstance().setCity(city);

        getURLContent(MeteoManager.getInstance().getCity());
    }

    public  void getURLContent(final String city) throws IOException {
        new ParseJSONFile() {
            @Override
            public void onPostExecute(String result) {
                JSONObject jsonObject = null;
                JSONArray jWeather = null;
                try {
                    jsonObject = new JSONObject(result);
                    int status = jsonObject.getInt("cod");
                    if (status ==200) {
                        jWeather = jsonObject.getJSONArray("weather");
                        String temp = jsonObject.getJSONObject("main").getString("temp");
                        double tempCelsius = Double.parseDouble(temp);
                        tempCelsius -= 272.15;
                        int tempCelsiuInteger = (int) tempCelsius;
                        String imageSrc = jWeather.getJSONObject(0).getString("icon");
                        String description = jWeather.getJSONObject(0).getString("description");
                        String name = jsonObject.getString("name");
                        City cityObj = new City(0, name);

                        Log.e(name.toLowerCase() , city.toLowerCase());
                        if (name.toUpperCase().equals(city.toUpperCase())) {
                            Meteo meteo = new Meteo(cityObj, tempCelsiuInteger + "", imageSrc, description);
                            MeteoManager.getInstance().setActualMeteo(meteo);

                            if (mAdapterViewPager.fragments.length > 0) {
                                ((MeteoFragment) mAdapterViewPager.fragments[0]).updateUI();
                                initiated = true;

                                if (!meteo.equals(R.string.search_hint)) {
                                    ((SearchFragment) mAdapterViewPager.fragments[1]).updateAdapterList(meteo);
                                }

                                dbHelper.createMeteoItem(meteo);
                            }
                            mViewPager.setCurrentItem(0);
                        }else
                            Toast.makeText(mContext,"City Not Found",Toast.LENGTH_LONG).show();

                    }
                    else{
                        ((SearchFragment) mAdapterViewPager.fragments[1]).removeItemSearch();
                    }
                } catch (JSONException e) {
                    Toast.makeText(mContext,"No Internet Connection",Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }

            }
        }.execute(city);
    }

    private void updateUIFromBD() {
        ArrayList<Meteo> meteoList = dbHelper.getAllMeteo();
        if (!meteoList.isEmpty()){
            MeteoManager.getInstance().setActualMeteo(meteoList.get(0));
            if (mAdapterViewPager.fragments.length>0)
                ((MeteoFragment) mAdapterViewPager.fragments[0]).updateUI();
        }
    }

    public void updateUIMeteoFragment() {
        System.out.println(mAdapterViewPager.fragmentManager);
        if (mAdapterViewPager.fragments.length > 0) {
            ((MeteoFragment) mAdapterViewPager.fragments[0]).updateUI();
            initiated = true;
        }
    }

    private class ParseJSONFile extends AsyncTask<String, Integer, String>
    {
        @Override
        protected String doInBackground(String... params) {
            String websiteName = "http://api.openweathermap.org/data/2.5/weather?q="+params[0];
            String json ="";
            try {
                json = getJSON(websiteName);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return json;
        }

        public String getJSON(String website) throws IOException {
            URL obj = new URL(website);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");

            int responseCode = con.getResponseCode();
            StringBuffer response = new StringBuffer();

            if (responseCode==200) {
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
            }

            return response.toString();
        }

    }
    public String getCurrentLocation() throws IOException {
        String currentLocationPlace = "";
        GPSTracker tracker = new GPSTracker(this);
        double lat = tracker.getLatitude();
        double lon = tracker.getLongitude();
        if (lat!=0.0 && lon !=0.0){
            CurrentLocation curr = new CurrentLocation(lat,lon);
            MeteoManager.getInstance().setCurrentLocation(curr);
            MeteoManager.getInstance().setTrackerLocation(tracker);
            currentLocationPlace = reverseGeocoder(new CurrentLocation(lat,lon));
        }
        tracker.stopUsingGPS();

        return currentLocationPlace;

    }

    public String reverseGeocoder(CurrentLocation currentLocation) throws IOException {
        Geocoder gc = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = gc.getFromLocation(currentLocation.getLat(), currentLocation.getLon(), 1);
        String city="";
        if (addresses.size() > 0) {
            Address address = addresses.get(0);
            city = address.getAdminArea();
        }

        return city;
    }


    private void initUIComponents() {
        mViewPager = (ViewPager) findViewById(R.id.vpPager);
        mAdapterViewPager = new AdapterViewPager(getSupportFragmentManager());
        mViewPager.setAdapter(mAdapterViewPager);
    }

    private void initComponents() {
        mContext = getApplicationContext();
        dbHelper = new DatabaseHelper(this);
        MeteoManager.getInstance().setContext(mContext);
        MeteoManager.getInstance().setSearchList(dbHelper.getAllMeteo());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            try {
                setMeteoInformationMenu();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public class AdapterViewPager extends PagerAdapter {
        FragmentManager fragmentManager;
        Fragment [] fragments;
        public AdapterViewPager(FragmentManager fm) {
            fragmentManager = fm;
            fragments = new Fragment[2];
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            assert(0 <= position && position < fragments.length);
            android.support.v4.app.FragmentTransaction trans = fragmentManager.beginTransaction();
            trans.remove(fragments[position]);
            trans.commit();
            fragments[position] = null;
        }

        @Override
        public Fragment instantiateItem(ViewGroup container, int position){
            Fragment fragment = getItem(position);
            android.support.v4.app.FragmentTransaction trans = fragmentManager.beginTransaction();
            trans.add(container.getId(),fragment,"fragment:"+position);
            trans.commit();
            return fragment;
        }

        @Override
        public int getCount() {
            return fragments.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object fragment) {
            return ((Fragment) fragment).getView() == view;
        }

        public Fragment getItem(int position){
            assert(0 <= position && position < fragments.length);
            if(fragments[position] == null){
                if (position==0) {
                    fragments[position] = MeteoFragment.newInstance(); //make your fragment here
                }
                else if (position==1){
                    fragments[position] = SearchFragment.newInstance(); //make your fragment here
                }
            }
            return fragments[position];
        }

        @Override
        public int getItemPosition(Object object){
            return POSITION_NONE;
        }


        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_meteo_fragment).toUpperCase(l);
                case 1:
                    return getString(R.string.title_search_fragment).toUpperCase(l);

            }
            return null;
        }

    }
}
