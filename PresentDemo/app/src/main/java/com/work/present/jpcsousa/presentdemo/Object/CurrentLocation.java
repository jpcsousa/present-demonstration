package com.work.present.jpcsousa.presentdemo.Object;

/**
 * Created by jpcsousa on 30/09/15.
 */
public class CurrentLocation {
    private double lat,lon;

    public CurrentLocation(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }
}
