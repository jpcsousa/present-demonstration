package com.work.present.jpcsousa.presentdemo.Manager;

import android.content.Context;

import com.work.present.jpcsousa.presentdemo.GPS.GPSTracker;
import com.work.present.jpcsousa.presentdemo.Object.CurrentLocation;
import com.work.present.jpcsousa.presentdemo.Object.Meteo;

import java.util.ArrayList;

/**
 * Created by jpcsousa on 30/09/15.
 */
public class MeteoManager {
    private static MeteoManager ourInstance = new MeteoManager();
    private Meteo actualMeteo = null;
    private static GPSTracker trackerLocation = null;
    private static CurrentLocation currentLocation = null;
    private static String city ="";
    private static String icon;
    private String description;
    private String temperature;
    private ArrayList<Meteo> searchList = new ArrayList<>();
    private Context context;

    public static MeteoManager getInstance() {
        return ourInstance;
    }

    private MeteoManager() {
    }

    public GPSTracker getTrackerLocation() {
        return trackerLocation;
    }

    public void setTrackerLocation(GPSTracker trackerLocation) {
        MeteoManager.trackerLocation = trackerLocation;
    }

    public void setCurrentLocation(CurrentLocation currentLocation) {
        MeteoManager.currentLocation = currentLocation;
    }

    public CurrentLocation getCurrentLocation() {
        return currentLocation;
    }

    public void setCity(String city) {
        MeteoManager.city = city;
    }

    public String getCity(){
        return city;
    }


    public void setActualMeteo(Meteo actualMeteo) {
        this.actualMeteo = actualMeteo;
    }

    public Meteo getActualMeteo() {
        return actualMeteo;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getIcon() {
        return icon;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getDescription() {
        return description;
    }

    public String getTemperature() {
        return temperature;
    }

    public ArrayList<Meteo> getSearchList() {
        return searchList;
    }

    public void setSearchList(ArrayList<Meteo> searchList) {
        this.searchList = searchList;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
