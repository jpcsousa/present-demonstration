package com.work.present.jpcsousa.presentdemo.Fragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.work.present.jpcsousa.presentdemo.Manager.MeteoManager;
import com.work.present.jpcsousa.presentdemo.Object.Meteo;
import com.work.present.jpcsousa.presentdemo.R;

import java.util.ArrayList;


public class MeteoFragment extends Fragment {

    private ImageView mMeteoImage;
    private TextView mTempState;
    private TextView mCity;
    private TextView mTemp;

    public static Fragment newInstance() {
        MeteoFragment fragment = new MeteoFragment();
        return fragment;
    }

    public MeteoFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_meteo, container, false);
        mMeteoImage = (ImageView) view.findViewById(R.id.meteoImage);
        mTempState = (TextView) view.findViewById(R.id.tempState);
        mCity = (TextView) view.findViewById(R.id.city);
        mTemp = (TextView) view.findViewById(R.id.temp);

        ArrayList<Meteo> meteoArrayList = MeteoManager.getInstance().getSearchList();
        if (!meteoArrayList.isEmpty())
            MeteoManager.getInstance().setActualMeteo(meteoArrayList.get(meteoArrayList.size()-1));
        updateUI();
        return view;
    }



    public void updateUI(){
        Meteo meteo = MeteoManager.getInstance().getActualMeteo();
        if (meteo!=null){
            Drawable image = getResourceName(meteo.getImageSrc());
            mMeteoImage.setImageDrawable(image);
            mTempState.setText(R.string.meteo_found);
            mCity.setText(meteo.getCity().getName());
            mTemp.setText(meteo.getTemp()+"ºC");
        }else{
            Drawable image = getResourceName("");
            mMeteoImage.setImageDrawable(image);
            mTempState.setText(R.string.meteo_not_found);
            mCity.setText(R.string.meteo_update);
            mTemp.setText("");
        }
    }


    public Drawable getResourceName(String res){
        Drawable drawable = getActivity().getResources().getDrawable(R.drawable.res_not_found);

        switch (res){
            case ("01d"):
                drawable = getActivity().getResources().getDrawable(R.drawable.res_01d);
                break;
            case ("01n"):
                drawable = getActivity().getResources().getDrawable(R.drawable.res_01n);
                break;
            case ("02d"):
                drawable = getActivity().getResources().getDrawable(R.drawable.res_02d);
                break;
            case ("02n"):
                drawable = getActivity().getResources().getDrawable(R.drawable.res_02n);
                break;
            case ("03d"):
                drawable = getActivity().getResources().getDrawable(R.drawable.res_03d);
                break;
            case ("03n"):
                drawable = getActivity().getResources().getDrawable(R.drawable.res_03n);
                break;
            case ("04d"):
                drawable = getActivity().getResources().getDrawable(R.drawable.res_04n);
                break;
            case ("04n"):
                drawable = getActivity().getResources().getDrawable(R.drawable.res_04n);
                break;
            case ("09d"):
                drawable = getActivity().getResources().getDrawable(R.drawable.res_09d);
                break;
            case ("09n"):
                drawable = getActivity().getResources().getDrawable(R.drawable.res_09n);
                break;
            case ("10d"):
                drawable = getActivity().getResources().getDrawable(R.drawable.res_10d);
                break;
            case ("10n"):
                drawable = getActivity().getResources().getDrawable(R.drawable.res_10n);
                break;
            case ("11d"):
                drawable = getActivity().getResources().getDrawable(R.drawable.res_11d);
                break;
            case ("11n"):
                drawable = getActivity().getResources().getDrawable(R.drawable.res_11n);
                break;
            case ("13d"):
                drawable = getActivity().getResources().getDrawable(R.drawable.res_13d);
                break;
            case ("13n"):
                drawable = getActivity().getResources().getDrawable(R.drawable.res_13n);
                break;
            case ("50d"):
                drawable = getActivity().getResources().getDrawable(R.drawable.res_50d);
                break;
            case ("50n"):
                drawable = getActivity().getResources().getDrawable(R.drawable.res_50n);
                break;
        }
        return drawable;
    }
}
