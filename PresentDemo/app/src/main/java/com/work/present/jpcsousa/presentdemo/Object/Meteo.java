package com.work.present.jpcsousa.presentdemo.Object;

/**
 * Created by jpcsousa on 30/09/15.
 */
public class Meteo {
    private City city;
    private String temp;
    private String imageSrc;
    private String description;

    public Meteo(City city, String temp, String imageSrc, String description) {
        this.city = city;
        this.temp = temp;
        this.imageSrc = imageSrc;
        this.description = description;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getImageSrc() {
        return imageSrc;
    }

    public void setImageSrc(String imageSrc) {
        this.imageSrc = imageSrc;
    }
}
